define("second", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SecondClass = /** @class */ (function () {
        function SecondClass() {
        }
        SecondClass.prototype.throwMethod = function () {
            throw new Error('someError!');
        };
        return SecondClass;
    }());
    exports.default = SecondClass;
});
define("index", ["require", "exports", "second"], function (require, exports, second_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SomeTypeScriptClass = /** @class */ (function () {
        function SomeTypeScriptClass() {
            this.publicNumber = 2;
            this.test = new second_1.default();
        }
        SomeTypeScriptClass.prototype.getSomeNumber = function () {
            this.test.throwMethod();
            return this.publicNumber + 2;
        };
        return SomeTypeScriptClass;
    }());
    var a = new SomeTypeScriptClass();
    setTimeout(function () {
        console.log('start-work!');
        var b = a.getSomeNumber();
        console.log(b);
    }, 2000);
});
//# sourceMappingURL=index.main.js.map