import SecondClass from './second';

class SomeTypeScriptClass {
    publicNumber:number = 2;
    test:SecondClass;

    constructor() {
        this.test = new SecondClass();
    }

    getSomeNumber():number {
        this.test.throwMethod(); //mark1
        return this.publicNumber + 2;
    }
}

let a = new SomeTypeScriptClass();

setTimeout(function() {
    console.log('start-work!');
    let b = a.getSomeNumber();
    console.log(b);

}, 2000);
